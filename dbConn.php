<?php 
	$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
	$servername = $url["host"];
	$username = $url["user"];
	$password = $url["pass"];
	$dbname = substr($url["path"], 1);
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	try {
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e) {
	    echo $sql . "<br>" . $e->getMessage();
    }
 ?>	

	