<?php 

	if(isset($_POST['addressID'])) {
	 	   
	    $sh_id = $_POST['addressID'];

	    include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr WHERE shaddr_id=:shaddr_id");
		$stmt -> bindParam(":shaddr_id", $bind_shaddr_id);
		$bind_shaddr_id = $sh_id;

		$stmt-> execute();

		$response = array();

		// Check if the result is correct
		if ($stmt->rowCount() == 1) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$response = $row;	
			$response[0] = $row['sh_firstname'];
			$response[1] = $row['sh_familyname'];
			$response[2] = $row['sh_phone'];
			$response[3] = $row['sh_street1'];
			$response[4] = $row['sh_street2'];
			$response[5] = $row['sh_city'];
			$response[6] = $row['sh_state'];
			$response[7] = $row['sh_postcode'];
			$response[8] = $row['sh_country'];
			$response[9] = $row['sh_company'];
			$response[10] = $row['sh_title'];
			echo json_encode($response);
		} 
		else {
			echo "0 results";
		}

		// Close the connection
		$stmt = null;
		$connection = null;
	}

 ?>