<?php  
	session_start();	
?>

<html>
 	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>COMP344 Assignment 2 2016, Checkout System [Core] </title>

	  	<meta charset="utf-8">
  	  	<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  		
		<script src="jquery-sdk/jquery-1.12.2.min.js"></script>
		<script src="jquery-sdk/jquery-ui.js"></script>
		<script>
			$(document).ready(function() {
				
			});
		</script>
		<script type="text/javascript">

			function validate() {

				var form = document.paymentForm;

				// ============================================================ //
				// VALIDATE CARD HOLDER
				// ============================================================ //
				var card_holder_name = form.card_holder_name.value;

				// Check if firstname field is not empty
				if (card_holder_name.length == 0) {
					alert("Card holder name field cannot be empty");
					return false;
				}

				// Check if firstname string includes only valid characters
				// First we specify string of all valid characters that can be entered by the user
				var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ._- ';

				// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
				for (var i = card_holder_name.length - 1; i >= 0; i--) {
				    if(validChars.indexOf(card_holder_name[i]) < 0) {
				        alert("Invalid character in card holder name field");
				        return false;
				    }
				}
				
				// ============================================================ //
				// VALIDATE CARD HOLDER
				// ============================================================ //
				var card_number = form.card_number.value;

				// Check if firstname field is not empty
				if (card_number.length == 0) {
					alert("Card number field cannot be empty");
					return false;
				}

				// Check if firstname string includes only valid characters
				// First we specify string of all valid characters that can be entered by the user
				var validChars = '0123456789';

				// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
				for (var i = card_number.length - 1; i >= 0; i--) {
				    if(validChars.indexOf(card_number[i]) < 0) {
				        alert("Invalid character in card number field");
				        return false;
				    }
				}				

				// ============================================================ //
				// VALIDATE EXPIRY DATE
				// ============================================================ //

				// var today;
				// var someday;
				// var expiry_month = form.expiry_month.value;
				// var expiry_year = form.expiry_year.value;
				// console.log('Expiry Month = '+ expiry_month);
				// console.log('Expiry Year = '+ expiry_year);
				
				// someday = new Date();
				// someday.setFullYear(expiry_year, expiry_month, 1);
				// 	today = new Date();
				// if (today.getTime() > someday.getTime()) {
			   	// 				alert("The expiry date is before today's date. Please select a valid expiry date");
			   	// 				return false;
				// }

				// ============================================================ //
				// VALIDATE CVV NUMBER
				// ============================================================ //
				var cvv = form.cvv.value;

				// Check if firstname field is not empty
				if (cvv.length == 0) {
					alert("CVV field cannot be empty");
					return false;
				}

				// Check if firstname string includes only valid characters
				// First we specify string of all valid characters that can be entered by the user
				var validChars = '0123456789';

				// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
				for (var i = cvv.length - 1; i >= 0; i--) {
				    if(validChars.indexOf(cvv[i]) < 0) {
				        alert("Invalid character in cvv field");
				        return false;
				    }
				}		

				return true;

			}
		</script>
 	</head>
 	<body>
 		<div class="jumbotron text-center">
  			<h1>Checkout</h1>
  			<h3>Payment</h3>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<!-- TODO: Add reference to the Shopping Cart System here-->
			    <li><a href="#">Cart</a></li>
			    <li><a href="index.php">Billing details</a></li>
			    <li><a href="shippingMethod.php">Payment & delivery</a></li>
			    <li><a href="shippingMethod.php">Order summary</a></li>
			    <li class="active">Payment</li>
			</ul>
			<form class="form-horizontal" role="form" method="post" action="processPayment.php" name="paymentForm"  onsubmit ='return validate()'>
			<fieldset>
			  <legend>Payment</legend>
			  <div class="form-group">
			    <label class="col-sm-3 control-label" for="card_holder_name">Name on Card</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" name="card_holder_name" id="card_holder_name" placeholder="Card Holder's Name">
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="col-sm-3 control-label" for="card_number">Card Number</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" name="card_number" id="card_number" placeholder="Debit/Credit Card Number">
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="col-sm-3 control-label" for="expiry_month">Expiration Date</label>
			    <div class="col-sm-9">
			      <div class="row">
			        <div class="col-xs-3">
			          <select class="form-control col-sm-2" name="expiry_month" id="expiry_month">
			            <option>Month</option>
			            <option value="01">Jan (01)</option>
			            <option value="02">Feb (02)</option>
			            <option value="03">Mar (03)</option>
			            <option value="04">Apr (04)</option>
			            <option value="05">May (05)</option>
			            <option value="06">June (06)</option>
			            <option value="07">July (07)</option>
			            <option value="08">Aug (08)</option>
			            <option value="09">Sep (09)</option>
			            <option value="10">Oct (10)</option>
			            <option value="11">Nov (11)</option>
			            <option value="12">Dec (12)</option>
			          </select>
			        </div>
			        <div class="col-xs-3">
			          <select class="form-control" name="expiry_year">
			            <option value="13">2013</option>
			            <option value="14">2014</option>
			            <option value="15">2015</option>
			            <option value="16">2016</option>
			            <option value="17">2017</option>
			            <option value="18">2018</option>
			            <option value="19">2019</option>
			            <option value="20">2020</option>
			            <option value="21">2021</option>
			            <option value="22">2022</option>
			            <option value="23">2023</option>
			          </select>
			        </div>
			      </div>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" name="cvv" id="cvv" placeholder="Security Code">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-3 col-sm-9">
			      <button type="submit" class="btn btn-success">Pay Now</button>
			    </div>
			  </div>
			</fieldset>
			</form>
		</div>
 	</body>
</html>