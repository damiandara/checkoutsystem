function validate() {

		var form = document.addressForm;

		var checkbox = form.shipping_checkbox;

		var isValid = true;
		// check whether user choosed different shipping address
		if (checkbox.checked) {
			// validate both addresses

			isValid = validateBillingAddress();

			if (isValid === false) {
				return false;
			}

			isValid = validateShippingAddress();

			if (isValid === false) {
				return false;
			}
		}
		else {
			// validate just billing address
			isValid = validateBillingAddress();

			if (isValid === false) {
				return false;
			}
		}
		
		return true;
}

function validateBillingAddress() {
	var form = document.addressForm;

		// ============================================================ //
		// VALIDATE FIRSTNAME
		// ============================================================ //
		var firstname = form.firstname.value;

		// Check if firstname field is not empty
		if (firstname.length == 0) {
			alert("Firstname field cannot be nil (Billing address)");
			return false;
		}

		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_- ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = firstname.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(firstname[i]) < 0) {
		        alert("Invalid character in firstname field (Billing address)");
		        return false;
		    }
		}
		
		// ============================================================ //
		// VALIDATE LASTNAME
		// ============================================================ //
		var lastname = form.lastname.value;

		// Check if firstname field is not empty
		if (lastname.length == 0) {
			alert("Lastname field cannot be nil (Billing address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_- ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = lastname.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(lastname[i]) < 0) {
		        alert("Invalid character in lastname field (Billing address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE ADDRESS1
		// ============================================================ //
		var address1 = form.address1.value;

		// Check if firstname field is not empty
		if (address1.length == 0) {
			alert("Address1 field cannot be nil (Billing address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-/ ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = address1.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(address1[i]) < 0) {
		        alert("Invalid character in address1 field (Billing address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE CITY
		// ============================================================ //
		var city = form.city.value;

		// Check if firstname field is not empty
		if (city.length == 0) {
			alert("City field cannot be nil (Billing address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = city.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(city[i]) < 0) {
		        alert("Invalid character in city field (Billing address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE POSTCODE
		// ============================================================ //
		var postcode = form.postcode.value;

		// Check if firstname field is not empty
		if (postcode.length < 4 || postcode.length > 5) {
			alert("Postcode field cannot be less than 4 digits or greater than 5 digits (Billing address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = postcode.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(postcode[i]) < 0) {
		        alert("Invalid character in postcode field (Billing address)");
		        return false;
		    }
		}

		return true;
}

function validateShippingAddress() {
		var form = document.addressForm;

		// ============================================================ //
		// VALIDATE FIRSTNAME
		// ============================================================ //
		var firstname = form.sh_firstname.value;

		// Check if firstname field is not empty
		if (firstname.length == 0) {
			alert("Firstname field cannot be nil (Shipping address)");
			return false;
		}

		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_- ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = firstname.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(firstname[i]) < 0) {
		        alert("Invalid character in firstname field (Shipping address)");
		        return false;
		    }
		}
		
		// ============================================================ //
		// VALIDATE LASTNAME
		// ============================================================ //
		var lastname = form.sh_lastname.value;

		// Check if firstname field is not empty
		if (lastname.length == 0) {
			alert("Lastname field cannot be nil (Shipping address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_- ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = lastname.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(lastname[i]) < 0) {
		        alert("Invalid character in lastname field (Shipping address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE ADDRESS1
		// ============================================================ //
		var address1 = form.sh_address1.value;

		// Check if firstname field is not empty
		if (address1.length == 0) {
			alert("Address1 field cannot be nil (Shipping address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-/ ';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = address1.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(address1[i]) < 0) {
		        alert("Invalid character in address1 field (Shipping address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE CITY
		// ============================================================ //
		var city = form.sh_city.value;

		// Check if firstname field is not empty
		if (city.length == 0) {
			alert("City field cannot be nil (Shipping address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = city.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(city[i]) < 0) {
		        alert("Invalid character in city field (Shipping address)");
		        return false;
		    }
		}

		// ============================================================ //
		// VALIDATE POSTCODE
		// ============================================================ //
		var postcode = form.sh_postcode.value;

		// Check if firstname field is not empty
		if (postcode.length < 4 || postcode.length > 5) {
			alert("Postcode field cannot be less than 4 digits or greater than 5 digits (Shipping address)");
			return false;
		}
	
		// Check if firstname string includes only valid characters
		// First we specify string of all valid characters that can be entered by the user
		var validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ.0123456789_-';

		// Then we loop through the firstname characters with the valid characters string to detect incorrect characters 
		for (var i = postcode.length - 1; i >= 0; i--) {
		    if(validChars.indexOf(postcode[i]) < 0) {
		        alert("Invalid character in postcode field (Shipping address)");
		        return false;
		    }
		}
		return true;
}