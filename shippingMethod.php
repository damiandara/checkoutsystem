<?php 
	session_start();

	$firstname = $lastname = $company = $street1 = $street2 = $postcode = $city = $state = $country = $title = "";
	// get variables from the current session 
	$shopper_id = $_SESSION['shopper_id'];
	$session_id = $_SESSION['session_id'];
	$order_id = $_SESSION['order_id'];	

	$shipping_id = "";

	check_session($session_id, $shopper_id);
	// check if user selected other shipping address than billing address
	if (isset($_POST['shipping_checkbox'])) {
		// set fields of shipping address form
		$firstname = test_input($_POST['sh_firstname']);  	
		$lastname = test_input($_POST['sh_lastname']);  	
		$company = test_input($_POST['sh_company']);  	
		$street1 = test_input($_POST['sh_address1']);  	
		$street2 = test_input($_POST['sh_address2']);  	
		$postcode = test_input($_POST['sh_postcode']);  	
		$city = test_input($_POST['sh_city']);  		
		$state = test_input($_POST['sh_state']);  	
		$country = test_input($_POST['sh_country']); 
		$title = test_input($_POST['sh_title']); 										
	}	
	else {
		// set fields of billing address
		$firstname = test_input($_POST['firstname']);  	
		$lastname = test_input($_POST['lastname']);  	
		$company = test_input($_POST['company']);  	
		$street1 = test_input($_POST['address1']);  	
		$street2 = test_input($_POST['address2']);  	
		$postcode = test_input($_POST['postcode']);  	
		$city = test_input($_POST['city']);  		
		$state = test_input($_POST['state']);  	
		$country = test_input($_POST['country']);  	
		$title = test_input($_POST['title']);									
	}

	// Update Orders table with the selected shipping and billing 
	// addresses in the previous step of checkout process

	$baddrID = test_input($_POST['billing_addr_select']);
	$shaddrID = test_input($_POST['shipping_addr_select']);

	/// CHANGE AFTER INTEGRATION
	$order_id = '12';

	updateOrderShippingAddresses($order_id, $baddrID, $shaddrID);

	// check if user chosen to save an address in the address book
	if (isset($_POST['save_address_checkbox'])) {
		include('dbConn.php');
		$connection = $conn;
		$stmt = $connection->prepare('INSERT INTO shaddr(shopper_id, sh_title, sh_firstname, sh_familyname, sh_street1, sh_street2, sh_city, sh_state, sh_postcode, sh_country)'.
		'VALUES (:shopper_id, :title, :firstname, :familyname, :street1, :street2, :city, :state, :postcode, :country)');
		$stmt->bindParam(":shopper_id", $bind_shopper_id);
		$stmt->bindParam(":title", $bind_title);
		$stmt->bindParam(":firstname", $bind_firstname);
		$stmt->bindParam(":familyname", $bind_familyname);
		$stmt->bindParam(":street1", $bind_street1);
		$stmt->bindParam(":street2", $bind_street2);
		$stmt->bindParam(":city", $bind_city);
		$stmt->bindParam(":state", $bind_state);
		$stmt->bindParam(":postcode", $bind_postcode);
		$stmt->bindParam(":country", $bind_country);

		$bind_shopper_id = $shopper_id;
		$bind_title = $title;
		$bind_firstname = $firstname;
		$bind_familyname = $lastname;
		$bind_street1 = $street1;
		$bind_street2 = $street2;
		$bind_city = $city;
		$bind_state = $state;
		$bind_postcode = $postcode;
		$bind_country = $country;

		$stmt->execute();
	   
  	 	$stmt = null;
		$connection = null;
	}

	function getOrderDetails($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM orders WHERE Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}

	// Check whether shopper
	function check_session($session_id, $shopper_id) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM session WHERE id = :session_id AND Shopper_id = :shopper_id;");
		$stmt -> bindParam(":session_id", $bind_session_id);
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_session_id = $session_id;
		$bind_shopper_id = $shopper_id;
		$stmt->execute();

		if ($stmt->rowCount() != 1) {
			// echo 'You are unauthorised';
			echo "<script type='text/javascript'>".
		 		"alert('Your session has expired and you will be redirected to the login page.');".
				"window.location.replace('https://google.com');". // CHANGE URL FOR SHOPPING CART SYSTEM URL
				"</script>";
		}

		$connection = null;
		$stmt = null;
	}

	// Test user input for occurance of harmful characters
	// Implementation based on http://www.w3schools.com/php/php_form_validation.asp
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	// update order with the selected shipping_address
	// TODO: need to add new field for storing billing address reference 
	function updateOrderShippingAddresses($order_id, $billing_address_id, $shipping_address_id) {
		
		if (!empty($billing_address_id)) {
			include('dbConn.php');
			$connection = $conn;
			if (empty($shipping_address_id)) {
				$stmt = $connection->prepare('UPDATE orders SET Order_Billaddr = :Order_Billaddr, Orded_Shaadr = :Order_Billaddr2 WHERE Order_id = :Order_id');	
				$stmt->bindParam(":Order_Billaddr2", $bind_billing_address2);
			}
			else {
				$stmt = $connection->prepare('UPDATE orders SET Orded_Shaadr = :Order_Shaadr, Order_Billaddr = :Order_Billaddr WHERE Order_id = :Order_id');		
				$stmt->bindParam(":Order_Shaadr", $bind_shopper_address);
			}
			
			
			$stmt->bindParam(":Order_Billaddr", $bind_billing_address);
			$stmt->bindParam(":Order_id", $bind_order_id);

			$bind_shopper_address = $shipping_address_id;
			$bind_billing_address = $billing_address_id;
			$bind_billing_address2 = $billing_address_id;
			$bind_order_id = $order_id;
			
			$stmt->execute();
		   
	  	 	$stmt = null;
			$connection = null;
		}
	}
	
	function saveShippingAddress($shopper_id) {
		include('dbConn.php');
		$connection = $conn;
		$stmt = $connection->prepare('INSERT INTO shaddr(shopper_id, sh_title, sh_firstname, sh_familyname, sh_street1, sh_street2, sh_city, sh_state, sh_postcode, sh_country)'.
		'VALUES (:shopper_id, :title, :firstname, :familyname, :street1, :street2, :city, :state, :postcode, :country)');
		$stmt->bindParam(":shopper_id", $bind_shopper_id);
		$stmt->bindParam(":title", $bind_title);
		$stmt->bindParam(":firstname", $bind_firstname);
		$stmt->bindParam(":familyname", $bind_familyname);
		$stmt->bindParam(":street1", $bind_street1);
		$stmt->bindParam(":street2", $bind_street2);
		$stmt->bindParam(":city", $bind_city);
		$stmt->bindParam(":state", $bind_state);
		$stmt->bindParam(":postcode", $bind_postcode);
		$stmt->bindParam(":country", $bind_country);

		$bind_shopper_id = $shopper_id;
		$bind_title = $title;
		$bind_firstname = $firstname;
		$bind_familyname = $lastname;
		$bind_street1 = $street1;
		$bind_street2 = $street2;
		$bind_city = $city;
		$bind_state = $state;
		$bind_postcode = $postcode;
		$bind_country = $country;

		$stmt->execute();
	   
  	 	$stmt = null;
		$connection = null;
	}

	function getShippingPostcode($order_id) {
		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT sh_postcode FROM shaddr AS s JOIN orders AS o ON (s.shaddr_id = o.Order_Billaddr) WHERE o.Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result['sh_postcode'];
		} 
		$connection = null;
		$stmt = null;
	}

	function fetchAvailableShippingOptions($fromPostcode, $toPostcode, $length, $width, $height, $weight, $current_service_code) {

		// Set your API key: remember to change this to your live API key in production
		$apiKey = getenv("AUS_POST_API_KEY");

		// Define the service input parameters
		$parcelLengthInCMs = $length;
		$parcelWidthInCMs = $width;
		$parcelHeighthInCMs = $height;
		$parcelWeightInKGs = $weight;

		// Set the query params
		$queryParams = array(
		  "from_postcode" => $fromPostcode,
		  "to_postcode" => $toPostcode,
		  "length" => $parcelLengthInCMs,
		  "width" => $parcelWidthInCMs,
		  "height" => $parcelHeighthInCMs,
		  "weight" => $parcelWeightInKGs
		);

		// Set the URL for the Domestic Parcel Size service
		$urlPrefix = 'digitalapi.auspost.com.au';
		$postageTypesURL = 'https://' . $urlPrefix . '/postage/parcel/domestic/service.json?' .
		http_build_query($queryParams);

		// Lookup available domestic parcel delivery service types
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postageTypesURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('AUTH-KEY: ' . $apiKey));
		$rawBody = curl_exec($ch);

		// Check the response; if the body is empty then an error has occurred
		if(!$rawBody){
		  die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
		}

		// All good, lets parse the response into a JSON object
		$serviceTypesJSON = json_decode($rawBody);

		$services = $serviceTypesJSON->services->service;

		foreach ($services as $service) {
			echo "<div class='form-check'><label class='form-check-label'>";
			echo "<input class='form-check-input' type='radio' name='shipping_code' id='exampleRadios2' value='". $service->code . "'";
			  if($service->code === $current_service_code) {
		 	    echo "checked "; 
		 	}
		  echo "/>". $service->name." AUD $".$service->price."</label></div>";
		}
	}
 ?>

<html>
 	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>COMP344 Assignment 2 2016, Checkout System [Core] </title>
	  	<meta charset="utf-8">
  	  	<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  		
		<script src="jquery-sdk/jquery-1.12.2.min.js"></script>
		<script src="jquery-sdk/jquery-ui.js"></script>
		<script>
			$(document).ready(function() {
				
				var text_max = 100;
				$('#count_message').html(text_max + ' remaining');

					$('#text').keyup(function() {
						var text_length = $('#text').val().length;
						var text_remaining = text_max - text_length;
				  
			  		$('#count_message').html(text_remaining + ' remaining');

				});

			  	$("#backBtn").on('click', function() {
					window.location.replace("index.php");
				})

			});
		</script>
		<script type="text/javascript">

			function validate() {
				return true;
			}

		</script>
 	</head>
 	<body>
 		<div class="jumbotron text-center">
  			<h1>Checkout</h1>
  			<h3>Payment & delivery</h3>
		</div>
		<div class="container">
				<ul class="breadcrumb">
					<!-- TODO: Add reference to the Shopping Cart System here-->
				    <li><a href="#">Cart</a></li>
				    <li><a href="index.php">Billing details</a></li>
				    <li class="active">Payment & delivery</li>
				    <li><a href="orderSummary.php">Order summary</a></li>
				    <li><a href="paymentMethod.php">Payment</a></li>
				</ul>
				<form name="paymentMethods" action="orderSummary.php" method="post">
				<div class="row">
					<div class="col-sm-3">
						<h4>Select your payment method</h4>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-check">
									<?php 
										// FIX ME
										//$order_id = $_SESSION['order_id']; 
										$order_id = '12';
										$order_details = getOrderDetails($order_id);
									?>
								  <label class="form-check-label">
								    <input class="form-check-input" type="radio" name="paymentType" id="exampleRadios1" value="1" 
								    <?php if($order_details['Order_PayMethod'] === '1') {
								    	echo "checked";
								    } ?>>
								    Credit Card
								  </label>
								</div>
								<div class="form-check">
								  <label class="form-check-label">
								    <input class="form-check-input" type="radio" name="paymentType" id="exampleRadios2" value="2"
								    <?php if($order_details['Order_PayMethod'] === '2') {
								    	echo "checked";
								    } ?>>
								    Debit Card
								  </label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<h4>Select your delivery method</h4>
						<div class="row">
							<div class= "col-sm-12">
								<div class="list-group">
								<?php
									$current_service_code = $order_details['Order_ShippingServiceCode'];
									$FROM_POSTCODE = getenv('AUS_POST_API_FROM_POSTCODE');
									$to_postcode = getShippingPostcode($order_id);

									fetchAvailableShippingOptions($FROM_POSTCODE, $to_postcode, 15, 4, 5,3.4, $current_service_code);
								 ?>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="billID" value="<?php echo $baddrID; ?>"/>
					<input type="hidden" name="shID" value="<?php echo $shaddrID; ?>"/>

					<div class="col-sm-4">
						<h4>Notes on delivery</h4>
						<div class="form-group">
								<textarea class="form-control" id="text" name="notes" 
								placeholder="If you have any delivery instructions or wish to add a message to a recipient, please enter these details here" rows="5" value="<?php 
									echo $order_details['Order_NoteForDelivery']; 
								?>">
								
							</textarea>
							<h6 class="pull-right" id="count_message"></h6>
						</div>
					</div>

					<div class="col-sm-12" style="height=60px;">
						<hr>
					</div>
							<input class='col-sm-4 btn btn-secondary' type="button" value="Back to billing details" id="backBtn">
								<div class="col-sm-4"></div>
								<input class='col-sm-4 btn btn-primary ' type="submit" value="Continue to order summary" id="continueBtn">
						
					</div>
				</form>
		</div>
 	</body>
</html>