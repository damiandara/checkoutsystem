

<?php
	// start session
	session_start();

	$order_id = "";
	$session_id = "";
	$shopper_id = "";
	$addresses_list = [];


	// Check if request sent to our system is of type POST
	if ($_SERVER["REQUEST_METHOD"] == "POST") {	

		// Set each variable with the received data 
		$order_id = test_input($_POST['order_id']);
		$session_id = test_input($_POST['session_id']);

		// Check whether received data is nil
		if ($order_id === NULL || $session_id === NULL) {
			// if so redirect user back to the Shopping Cart System
		 	echo "<script type='text/javascript'>".
			 		"alert('Something went wrong make sure your shopping cart system sends proper values to our checkout system:)');".
					"window.location.replace('https://google.com');". // <<== CHANGE URL FOR SHOPPING CART SYSTEM URL
					"</script>";
		}
		else {
			// store current ids in the session
			$_SESSION['order_id'] = $order_id;
			$_SESSION['session_id'] = $session_id;

			check_session($session_id);
		}
	}

	// Test user input for occurance of harmful characters
	// Implementation based on http://www.w3schools.com/php/php_form_validation.asp
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	// Check whether shopper
	function check_session($session_id, $shopper_id) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM session WHERE id = :session_id AND Shopper_id = :shopper_id;");
		$stmt -> bindParam(":session_id", $bind_session_id);
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_session_id = $session_id;
		$bind_shopper_id = $shopper_id;
		$stmt->execute();

		if ($stmt->rowCount() != 1) {
			// echo 'You are unauthorised';
			echo "<script type='text/javascript'>".
		 		"alert('Your session has expired and you will be redirected to the login page.');".
				"window.location.replace('https://google.com');". // CHANGE URL FOR SHOPPING CART SYSTEM URL
				"</script>";
		}

		$connection = null;
		$stmt = null;
	}

	// Get shopper's  billing and shipping addresses 
	function getShippingAddress($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr AS s JOIN orders AS o ON (s.shaddr_id = o.Orded_Shaadr) WHERE o.Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}

	// Get shopper's  billing and shipping addresses 
	function getBillingAddress($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr AS s JOIN orders AS o ON (s.shaddr_id = o.Order_Billaddr) WHERE o.Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}
	
	// Returns Order_Shopper id from the Orders table
	function getShopperID($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT Order_Shopper FROM orders WHERE Order_id = :Order_id");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		// Check if the result is correct
		if ($stmt->rowCount() == 1) {
			$check = $stmt->fetch(PDO::FETCH_ASSOC);
			$result = $check['Order_Shopper'];
			$_SESSION['shopper_id'] = $result;
			return $result;
		} 
		
		$stmt = null;
		$connection = null;
	}

	// Returns list of shopper addressses based on shopper_id passed to the function
	function getShopperAddresses($shopper_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr WHERE shopper_id= :Shopper_id");
		$stmt -> bindParam(":Shopper_id", $bind_shopper_id);
		$bind_shopper_id = $shopper_id;

		$stmt -> execute();

		// Check if number of rows returned is more than 0
		if ($stmt->rowCount() > 0) {
			$result = $stmt->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);	

			foreach ($result as $key => $value) {
				
			}

			return $result;
		} 
		
		// close connection
		$stmt = null;
		$connection = null;
		
	}	

	// Those two fields have been hardcoded for testing purposes 
	// and should be removed after integrating with other systems
	// also session are handled during sending in 
	$session_id = '2';
	$order_id = '12';
	$_SESSION['order_id'] = $order_id;
	$_SESSION['session_id'] = $session_id;

	// Get shopper id from the order details
	$shopper_id = getShopperID($order_id);

	// Check whether current session is active
	check_session($session_id, $shopper_id);

	// Get list of addresses 
	$addresses_list = getShopperAddresses($shopper_id);

?>
<html>
 	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>COMP344 Assignment 2 2016, Checkout System [Core] </title>

	  	<meta charset="utf-8">
  	  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="jquery-sdk/jquery-1.12.2.min.js"></script>
		<script src="jquery-sdk/jquery-ui.js"></script>

		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function() {
					$('#shipping_checkbox').click(function() {
					    if(this.checked) {
					    	$("#sh_title").prop('disabled', false);
					    	$("#sh_firstname").prop('disabled', false);
					    	$("#sh_lastname").prop('disabled', false);
					    	$("#sh_company").prop('disabled', false);
					    	$("#sh_state").prop('disabled', false);
					    	$("#sh_address1").prop('disabled', false);
					    	$("#sh_address2").prop('disabled', false);
					    	$("#sh_city").prop('disabled', false);
					    	$("#sh_state").prop('disabled', false);
					    	$("#sh_postcode").prop('disabled', false);
					    	$("#sh_country").prop('disabled', false);
					    	$("#sh_phone").prop('disabled', false);
					    	$('#shipping_addr_select').prop('disabled', false);
					    	$('#sh_save').prop('disabled', false);
					    }
					    else {
					    	$("#sh_title").prop('disabled', true);
					    	$("#sh_firstname").prop('disabled', true);
					    	$("#sh_lastname").prop('disabled', true);
					    	$("#sh_company").prop('disabled', true);
					    	$("#sh_address1").prop('disabled', true);
					    	$("#sh_address2").prop('disabled', true);
					    	$("#sh_city").prop('disabled', true);
					    	$("#sh_state").prop('disabled', true);
					    	$("#sh_postcode").prop('disabled', true);
					    	$("#sh_country").prop('disabled', true);
					    	$("#sh_phone").prop('disabled', true);
					    	$('#shipping_addr_select').prop('disabled', true);
					    	$('#sh_save').prop('disabled', true);
					    }
					});

					$("#backBtn").on('click', function() {
						// TODO: 
						//window.location.replace("index.php");
					})

					$("#billing_addr_select").on('change', function() {
						var selectedValue = $(this).val();
						console.log("Selected value = " + selectedValue);
						if (selectedValue != 'new') {
							$.ajax({
				                type: "POST",
				                dataType: 'json',
				                url: "fetchAddress.php",
				                data: { addressID: selectedValue },
				                success: function(data) {
					    		    $("#firstname").val(data[0]);
							    	$("#lastname").val(data[1]);
							    	$("#phone").val(data[2]);
							    	$("#address1").val(data[3]);
							    	$("#address2").val(data[4]);
							    	$("#city").val(data[5]);
							    	$("#state").val(data[6]);
							    	$("#postcode").val(data[7]);
							    	$("#country").val(data[8]);
							    	$("#company").val(data[9]);
							    	$("#title").val(data[10]);
							    }
				            })	
						}
						else {
							$("#firstname").val("");
							$("#lastname").val("");
							$("#phone").val("");
							$("#address1").val("");
							$("#address2").val("");
							$("#city").val("");
							$("#state").val("");
							$("#postcode").val("");
							$("#country").val("");
							$("#company").val("");
							$("#title").val("");
						}
					    
					})

					$("#shipping_addr_select").on('change', function() {
						var selectedValue = $(this).val();
						if (selectedValue != 'new') {
							$.ajax({
				                type: "POST",
				                dataType: 'json',
				                url: "fetchAddress.php",
				                data: { addressID: selectedValue },
				                success: function(data) {
					    		    $("#sh_firstname").val(data[0]);
							    	$("#sh_lastname").val(data[1]);
							    	$("#sh_phone").val(data[2]);
							    	$("#sh_address1").val(data[3]);
							    	$("#sh_address2").val(data[4]);
							    	$("#sh_city").val(data[5]);
							    	$("#sh_state").val(data[6]);
							    	$("#sh_postcode").val(data[7]);
							    	$("#sh_country").val(data[8]);
							    	$("#sh_company").val(data[9]);
							    	$("#sh_title").val(data[10]);
							    }
				            })	
						}
						else {
							$("#sh_firstname").val("");
							$("#sh_lastname").val("");
							$("#sh_phone").val("");
							$("#sh_address1").val("");
							$("#sh_address2").val("");
							$("#sh_city").val("");
							$("#sh_state").val("");
							$("#sh_postcode").val("");
							$("#sh_country").val("");
							$("#sh_company").val("");
						}
					    
					})
			});
		</script>
		<script type="text/javascript" src="addressValidation.js"></script>
 	</head>
 	<body>
 		
 		<div class="jumbotron text-center">
  			<h1>Checkout</h1>
  			<h3>Billing details</h3>
		</div>
		<div class="container">
				<ul class="breadcrumb">
					<!-- TODO: Add reference to the Shopping Cart System here-->
				    <li><a href="#">Cart</a></li>
				    <li class="active">Billing details</li>
				    <li><a href="shippingMethod.php">Payment & delivery</a></li>
				    <li><a href="orderSummary.php">Order summary</a></li>
				    <li><a href="paymentMethod.php">Payment</a></li>
				</ul>
				<form name="addressForm" action="shippingMethod.php" method="post" onsubmit ='return validate()'>
  				<div class="row">
    				<div class="col-sm-6">
      					<h3>BILLING ADDRESS</h3>
						<hr>
						<div class="row">
		  					<div class="col-sm-12">
		  						<p>Select billing address from the list or enter new address</p>
		  						<div class="form-group" >
		  								<select class="form-control" data-style="btn-primary" id="billing_addr_select" name="billing_addr_select">
								        	<?php  
								        		foreach ($addresses_list as $key => $al) { ?>
													<option value= "<?php echo $key;?> "> <?php echo $al['sh_title']; echo $al['shaddr_id']; ?> </option>
											<?php } ?>
								        	<option value="new"> ADD NEW ADDRESS </option>
							        	</select>
						      	</div>
						    </div>
	  					</div>
	  					<?php
				    		$billing_address = getBillingAddress($order_id);
						?>
	  					<div class="form-group">
					      <input type="text" class="form-control" name="title" id="title" placeholder="Address title"
					      value="<?php echo $billing_address['sh_title']; ?>">
					    </div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
							      	<input type="text" class="form-control"  name="firstname" id="firstname" placeholder="First name *" 
							      	value="<?php echo $billing_address['sh_firstname']; ?>">
							    </div>		
							</div>
							<div class="col-sm-6">
								<div class="form-group">
							      <input type="text" class="form-control"  name="lastname" id="lastname" placeholder="Last name *" 
							      value="<?php echo $billing_address['sh_familyname']; ?>"> 
							    </div>	
							</div>
						</div>
					    <div class="form-group">
					      <input type="text" class="form-control" name="company" id="company" placeholder="Company"
					      value="<?php echo $billing_address['sh_company']; ?>">
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" name="address1" id="address1" placeholder="Address line 1" 
					      value="<?php echo $billing_address['sh_street1']; ?>">
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" name="address2" id="address2" placeholder="Address line 2" 
					      value="<?php echo $billing_address['sh_street2']; ?>">
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" name="city" id="city" placeholder="City *" 
					      value="<?php echo $billing_address['sh_city']; ?>">
					    </div>
					    <div class="row">
					    	<div class="col-sm-6">
					    		<div class="form-group">
							      	<input type="text" class="form-control" name="state" id="state" placeholder="State *" 
							      	value="<?php echo $billing_address['sh_state']; ?>">
							    </div>		
					    	</div>
					    	<div class="col-sm-6">
					    		<div class="form-group">
							      <input type="text" name="postcode" class="form-control" id="postcode" placeholder="Postcode *" 
							      value="<?php echo $billing_address['sh_postcode']; ?>">
							    </div>		
					    	</div>
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="country" name="country" placeholder="Country *" 
					      value="<?php echo $billing_address['sh_country']; ?>">
					    </div>
					    <div class="checkbox">
					      <label><input id="shipping_checkbox" name="shipping_checkbox" type="checkbox">SHIP TO A DIFFERENT ADDRESS</label>
					    </div>
					    <p>* This is requierd field</p>
    				</div>
    			<div class="col-sm-6">
  					<h3>SHIPPING ADDRESS</h3>
  					<hr>
  					<div class="row">
	  					<div class="col-sm-12">
	  						<p>Select shipping address from the list or enter new address</p>
	  						<div class="form-group" >
						        <select class="form-control" data-style="btn-primary" name="shipping_addr_select" id="shipping_addr_select" disabled>
								  	<?php  
						        		foreach ($addresses_list as $key => $al) { ?>
											<option value= "<?php echo $key;?> "> <?php echo $al['sh_title']; echo $al['shaddr_id']; ?> </option>
										<?php } ?>
							        	<option value="new"> ADD NEW ADDRESS </option>
								</select>	
					      	</div>
					    </div>
  					</div>

  					<?php
			    		$shipping_address = getShippingAddress($order_id);
					?>
					<div class="form-group">
			    	    <input type="text" class="form-control" name="sh_title" id="sh_title" placeholder="Address title" 
			    	    value="<?php echo $shipping_address['sh_title']; ?>" disabled>
				    </div>
	 				<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
							      	<input type="text" class="form-control" id="sh_firstname" name="sh_firstname" placeholder="FIRST NAME *" 
							      	value="<?php echo $shipping_address['sh_firstname']; ?>" disabled>
							    </div>		
							</div>
							<div class="col-sm-6">
								<div class="form-group">
							      <input type="text" class="form-control" id="sh_lastname" name="sh_lastname" placeholder="LAST NAME *" value="<?php echo $shipping_address['sh_familyname']; ?>" disabled>
							    </div>	
							</div>
						</div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="sh_company" name="sh_company" placeholder="COMPANY"
					      value="<?php echo $shipping_address['sh_company']; ?>" disabled>
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="sh_address1" name="sh_address1" placeholder="Address line 1"
					      value="<?php echo $shipping_address['sh_street1']; ?>" disabled>
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="sh_address2" name="sh_address2" placeholder="Address line 2 " 
					      value="<?php echo $shipping_address['sh_street2']; ?>" disabled>
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="sh_city" name="sh_city" placeholder="City *"
					      value="<?php echo $shipping_address['sh_city']; ?>" disabled>
					    </div>
					    <div class="row">
					    	<div class="col-sm-6">
					    		<div class="form-group">
							      	<input type="text" class="form-control" id="sh_state" name="sh_state" placeholder="State *"
							      	value="<?php echo $shipping_address['sh_state']; ?>" disabled>
							    </div>		
					    	</div>
					    	<div class="col-sm-6">
					    		<div class="form-group">
							      <input type="text" class="form-control" id="sh_postcode" name="sh_postcode" placeholder="POSTCODE *"
							      value="<?php echo $shipping_address['sh_postcode']; ?>" disabled>
							    </div>		
					    	</div>
					    </div>
					    <div class="form-group">
					      <input type="text" class="form-control" id="sh_country" name="sh_country" placeholder="COUNTRY *" 
					      value="<?php echo $shipping_address['sh_country']; ?>" disabled>
					    </div>
					     <div class="checkbox">
					      <label><input type="checkbox" id="sh_save" name="save_address_checkbox">SAVE IN ADDRESS BOOK</label>
					    </div>
    				</div>
				</div>
				<div class='row'>
			   		<input class='col-sm-4 btn btn-secondary' type="button" value="Back to Cart" id="backBtn">
					<div class='col-sm-4'></div>
					<input class='col-sm-4 btn btn-primary' type="submit" value="Continue to Payment & delivery">
				</div>	
				<div style="height:60px; width: 100%;"></div>
			</div>
			</form>
		</div>
 	</body>
 </html>	