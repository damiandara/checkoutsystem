<?php 
	
	session_start();

	$order_id = $_SESSION['order_id'];
	$shopper_id = $_SESSION['shopper_id'];
	$session_id = $_SESSION['session_id'];

	check_session($session_id, $shopper_id);

	// Check whether shopper
	function check_session($session_id, $shopper_id) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM session WHERE id = :session_id AND Shopper_id = :shopper_id;");
		$stmt -> bindParam(":session_id", $bind_session_id);
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_session_id = $session_id;
		$bind_shopper_id = $shopper_id;
		$stmt->execute();

		if ($stmt->rowCount() != 1) {
			// echo 'You are unauthorised';
			echo "<script type='text/javascript'>".
		 		"alert('Your session has expired and you will be redirected to the login page.');".
				"window.location.replace('https://google.com');". // CHANGE URL FOR SHOPPING CART SYSTEM URL
				"</script>";
		}

		$connection = null;
		$stmt = null;
	}


	function getOrderTotal($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT Order_Total FROM orders WHERE Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result['Order_Total'];
		} 
		$connection = null;
		$stmt = null;
	}

	function getShopperInfo($shopper_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shopper WHERE shopper_id = :shopper_id;");
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_shopper_id = $shopper_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt -> fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}


	function updateOrderDetails($order_id , $paid, $pay_date) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare('UPDATE orders SET Order_Paid = :Order_Paid, Order_PayDate = :Order_PayDate WHERE Order_id = :Order_id');
		// $stmt->bindParam(":Order_PaymentAuthorised", $bind_authorised);
		// $stmt->bindParam(":Order_PaymentPAN", $bind_pan);
		$stmt->bindParam(":Order_Paid", $bind_paid);
		$stmt->bindParam(":Order_PayDate", $bind_pay_date);
		$stmt->bindParam(":Order_id", $bind_order_id);

		$bind_authorised = $authorised;
		$bind_pan = $pan;
		$bind_paid = $paid;
		$bind_pay_date = $pay_date;
		$bind_order_id = $order_id;
		
		$stmt->execute();
	   
  	 	$stmt = null;
		$connection = null;
	}
	
	$card_holder_name = htmlspecialchars($_POST["card_holder_name"]);
	$card_number = htmlspecialchars($_POST["card_number"]);
	$expiry_month = htmlspecialchars($_POST["expiry_month"]);
	$expiry_year = htmlspecialchars($_POST["expiry_year"]);
	$cvv = htmlspecialchars($_POST["cvv"]); 
	
	// Metchant information stored as an env values on the server
	$merchant_id = getenv('MERCHANT_ID');
	$merchant_pwd = getenv('MERCHANT_PASSWORD');
	

	// Uncomment it in the production environment
	// =============================================================
	// GET TOTAL AMOUNT TO BE PAID FROM THE ORDERS TABLE
	// $order_total = getOrderTotal($order_id);
	// $amount = $order_total;
	$amount = "200"; // <--- REPLACE THIS VALUE AFTER INTEGRATION 
	                 //      WITH order_total
	// =============================================================
	
	$currency = "AUD";
	$purchase_order_no = "test";

  	$postFields = "<?xml version='1.0' encoding='UTF-8'?> 
			<SecurePayMessage>
				<MessageInfo> 
			      <messageID>8af793f9af34bea0cf40f5fb750f64</messageID> 
			      <messageTimestamp>20042303111214383000+660</messageTimestamp> 
			      <timeoutValue>60</timeoutValue> 
			      <apiVersion>xml-4.2</apiVersion>
			  	</MessageInfo>
			    <MerchantInfo>
					<merchantID>".$merchant_id."</merchantID>
					<password>".$merchant_pwd."</password> 
			  	</MerchantInfo> 
			  	<RequestType>Payment</RequestType> 
			  	<Payment>
			      <TxnList count='1'>
				     <Txn ID='1'>
			           <txnType>0</txnType> 
			           <txnSource>23</txnSource> 
			           <amount>".$amount."</amount> 
			           <recurring>no</recurring> 
			           <currency>".$currency."</currency> 
			           <purchaseOrderNo>".$purchase_order_no."</purchaseOrderNo> 
			           <CreditCardInfo>
			             <cardNumber>".$card_number."</cardNumber> 
			             <expiryDate>".$expiry_month."/".$expiry_year."</expiryDate> 
			             <cvv>".$cvv."</cvv>
			           </CreditCardInfo>
			        </Txn>
			      </TxnList>
				 </Payment>
			</SecurePayMessage>";

	$url = 'https://test.securepay.com.au/xmlapi/payment';
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );
	$result = curl_exec($ch);
	curl_close($ch);

	$xml=simplexml_load_string($result);
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	$statusCode =  $array['Status']['statusCode'];
	$statusDescription = $array['Status']['statusDescription'];
	$paymentResponseCode = $array['Payment']['TxnList']['Txn']['responseCode'];
	$paymentResponseText = $array['Payment']['TxnList']['Txn']['responseText'];
	$approved = $array['Payment']['TxnList']['Txn']['approved'];
	$settlementDate = $array['Payment']['TxnList']['Txn']['settlementDate'];

	//echo $statusCode . " " . $statusDescription . " " . $responseCode . " " . $paymentResponseCode . " " . $paymentResponseText . " Approved: " . $approved;

	?>
	<html>
	 	<head>
			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
			<title>COMP344 Assignment 2 2016, Checkout System [Core] </title>
		  	<meta charset='utf-8'>
	  	  	<meta name='viewport' content='width=device-width, initial-scale=1'>
	  		<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>
	  		
			<script src='jquery-sdk/jquery-1.12.2.min.js'></script>
			<script src='jquery-sdk/jquery-ui.js'></script>
			<script>
				$(document).ready(function() {
					$('#backBtn').on('click', function() {
						window.location.replace('index.php');
					})				
					$('#againBtn').on('click', function() {
						window.location.replace('index.php');
					})					
				});
			</script>
			<script type='text/javascript'>

			</script>
	 	</head>
	 	<body>
	 		<div class='jumbotron text-center'>
	  			<h1>Checkout</h1>
	  			<h3>Order Confirmation</h3>
			</div>
			<div class='container'>
				<?php 


					if($approved === 'Yes') {
						
						$shopper = getShopperInfo($shopper_id);
						updateOrderDetails($order_id , 1, $settlementDate);

						echo "<div class='container'>
							<div class='row'>
								<div class='col-sm-12'>
									<h2>Thank you. Your order is complete.</h2>
								</div>
								<div class='col-sm-12'>
									<h4>Your order number is: #12345</h4>
									<p>An order confirmation will be sent to: ". $shopper['sh_email'] . "</p>
								</div>
								<div class='col-sm-4'>
									<div class='row'>
										<div class='col-sm-12' style='margin-top: 30px;'>
											<input class='btn btn-primary' type='button' value='Go Back to Home Page' id='backBtn'>
										</div>
									</div>
								</div>
							</div>
						</div>";

						// send confirmation to the user
				 		include('confirmation.php');
					}
					else {
						echo "
						<div class='container'>
							<div class='row'>
								<div class='col-sm-12'>
									<h2>You payment could not be complete.</h2>
								</div>
								<div class='col-sm-12'>
									<h4>".$paymentResponseText."</h4>
								</div>
								<div class='col-sm-4'>
									<input class='btn btn-secondary' type='button' value='Try Again' id='againBtn'>
								</div>
							</div>
						</div>";
					}
				 ?>
			</div>
	 	</body>
	</html>