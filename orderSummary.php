<?php 
	session_start();

	$paymentMethod = test_input($_POST['paymentType']);
	$shippingCode = test_input($_POST['shipping_code']);
	$deliveryNotes = test_input($_POST['notes']);
	$shAddrID = test_input($_POST['shID']);
	$bAddrID = test_input($_POST['billID']);
	
	$order_id = $_SESSION['order_id'];
	$session_id = $_SESSION['session_id'];
	$shopper_id = $_SESSION['shopper_id'];

	check_session($session_id, $shopper_id);
	changeOrderDetails($order_id, $deliveryNotes, $paymentMethod, $shippingCode);

	// Test user input for occurance of harmful characters
	// Implementation based on http://www.w3schools.com/php/php_form_validation.asp
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	// Update order table with the values from the previous step of checkout process
	function changeOrderDetails($order_id, $delivery_notes, $payment_method, $shipping_code) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare('UPDATE orders SET Order_NoteForDelivery = :Order_NoteForDelivery, Order_PayMethod = :Order_PayMethod, Order_ShippingServiceCode = :Order_ShippingServiceCode WHERE Order_id = :Order_id');
		$stmt->bindParam(":Order_NoteForDelivery", $bind_note_for_delivery);
		$stmt->bindParam(":Order_PayMethod", $bind_pay_method);
		$stmt->bindParam(":Order_ShippingServiceCode", $bind_shipping_code);
		$stmt->bindParam(":Order_id", $bind_order_id);

		$bind_note_for_delivery = $delivery_notes;
		$bind_shipping_code = $shipping_code;
		$bind_pay_method = $payment_method;
		$bind_order_id = $order_id;
		
		$stmt->execute();
	   
  	 	$stmt = null;
		$connection = null;
	}

	// Check whether shopper
	function check_session($session_id, $shopper_id) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM session WHERE id = :session_id AND Shopper_id = :shopper_id;");
		$stmt -> bindParam(":session_id", $bind_session_id);
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_session_id = $session_id;
		$bind_shopper_id = $shopper_id;
		$stmt->execute();

		if ($stmt->rowCount() != 1) {
			// echo 'You are unauthorised';
			echo "<script type='text/javascript'>".
		 		"alert('Your session has expired and you will be redirected to the login page.');".
				"window.location.replace('https://google.com');". // CHANGE URL FOR SHOPPING CART SYSTEM URL
				"</script>";
		}

		$connection = null;
		$stmt = null;
	}

	// Fetch shopper info in order to display it in order summary page
	function fetchShopperInfo($shopperID) {

		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shopper WHERE shopper_id=:shopper_id");
		$stmt -> bindParam(":shopper_id", $bind_shopper_id);
		$bind_shopper_id = $shopperID;

		$stmt->execute();

		// Check if the result is correct
		if ($stmt->rowCount() == 1) {
			$check = $stmt->fetch(PDO::FETCH_ASSOC);
			// Display email and phone in the order summary page
			$email = $check['sh_email'];
			$phone = $check['sh_phone'];
			echo "<div class='col-sm-12'>
					<label>Email Address</label>
					<p>".$email."</p>
				</div>
				<div class='col-sm-12'>
					<label>Phone number</label>
					<p>".$phone."</p>
				</div>";
		} 
	}

	// Get shopper's  billing and shipping addresses 
	function getShippingAddress($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr AS s JOIN orders AS o ON (s.shaddr_id = o.Orded_Shaadr) WHERE o.Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}

	// Get shopper's  billing and shipping addresses 
	function getBillingAddress($order_id) {
		include("dbConn.php"); 
		$connection = $conn;

		$stmt = $connection->prepare("SELECT * FROM shaddr AS s JOIN orders AS o ON (s.shaddr_id = o.Order_Billaddr) WHERE o.Order_id = :Order_id;");
		$stmt -> bindParam(":Order_id", $bind_order_id);
		$bind_order_id = $order_id;

		$stmt->execute();

		if ($stmt->rowCount() == 1) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			return $result;
		} 
		$connection = null;
		$stmt = null;
	}

	// Calculate shipping cost by using AUSPOST API
	function calculateShippingCost($from, $to, $length, $width, $height, $weight, $service_code) {
		// Set your API key: remember to change this to your live API key in production
		$apiKey = getenv("AUS_POST_API_KEY");
		// Set the query params
		$queryParams = array(
		  "from_postcode" => $from,
		  "to_postcode" => $to,
		  "length" => $length,
		  "width" => $width,
		  "height" => $height,
		  "weight" => $weight,
		  "service_code" => $service_code
		);

		// Set the URL for the Domestic Parcel Calculation service
		$urlPrefix = 'digitalapi.auspost.com.au';
		$calculateRateURL = 'https://' . $urlPrefix . '/postage/parcel/domestic/calculate.json?' .
		http_build_query($queryParams);

		// Calculate the final domestic parcel delivery price
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $calculateRateURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('AUTH-KEY: ' . $apiKey));
		$rawBody = curl_exec($ch);

		// Check the response; if the body is empty than an error has occurred
		if(!$rawBody){
		  die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
		}

		// All good, lets parse the response into a JSON object
		$priceJSON = json_decode($rawBody);

		$service = $priceJSON->postage_result->service;
		$delivery_time =  $priceJSON->postage_result->delivery_time;
		$cost = $priceJSON->postage_result->total_cost;

		echo $service . "<br><b>Delivery time:</b>" . $delivery_time . "<br><b>Shipping Cost:</b> $" . $cost;

		return $cost;
	}

	// Format address to handle empty fields in the database
	function formatAddress($address_array) {
		$v = $address_array;
		$address = "";

	    if (!empty($v['sh_street1'])) {
	    	$address .= $v['sh_street1'];
	    	$address .= " ";
	    }

	    if (!empty($v['sh_street2'])) {
	    	$address .= $v['sh_street2'];
	    	$address .= " ";
	    }

	    if (!empty($v['sh_postcode'])) {
	    	$address .= $v['sh_postcode'];
	    	$address .= " ";
	    }

	    if (!empty($v['sh_city'])) {
	    	$address .= $v['sh_city'];
	    	$address .= " ";
	    }

	    if (!empty($v['sh_state'])) {
	    	$address .= $v['sh_state'];
	    	$address .= " ";
	    }

		return $address;
	}

	// Update values of TotalPrice, TaxAmount and ProductAmount fields
	// Not sure if my system should be doing it but I needed it for the testing purposes
	function updateOrderTotal($order_id, $total_price, $tax_amount, $product_amount) {

		include('dbConn.php');
		$connection = $conn;

		$stmt = $connection->prepare('UPDATE orders SET Order_Total = :Order_Total, Order_TaxAmount = :Order_TaxAmount, Order_ProductAmount = :Order_ProductAmount WHERE Order_id = :Order_id');
		$stmt->bindParam(":Order_Total", $bind_order_total);
		$stmt->bindParam(":Order_TaxAmount", $bind_tax_amount);
		$stmt->bindParam(":Order_ProductAmount", $bind_product_amount);
		$stmt->bindParam(":Order_id", $bind_order_id);

		$bind_order_total = $total_price;
		$bind_tax_amount = $tax_amount;
		$bind_product_amount = $product_amount;
		$bind_order_id = $order_id;
		
		$stmt->execute();
	   
  	 	$stmt = null;
		$connection = null;
	}

	// Calculate total tax amount of the order
	function calculateTaxAmount($products) {
		$totalTax = 0;
		foreach ($products as $key => $value) {
			$price=$value['PrPr_Price'];
			$taxRate=10;
			$tax=$price*$taxRate/100;
			$totalTax += $tax;
		}
		return $totalTax;
	}

	// Calculate total price
	function calculateTotalAmount($products, $shipping_cost) {
		$totalAmount = 0;
		foreach ($products as $key => $value) {
			$price=$value['PrPr_Price'];
			$taxRate=10;
			$tax=$price*$taxRate/100;
			$total=$price+$tax;
			$totalAmount += $total;
		}
		$totalAmount += $shipping_cost;
		return $totalAmount;
	}

	// Calculate subtotal price
	function calculateSubtotal($products) {
		$subtotal = 0;
		foreach ($products as $key => $value) {
			$price=$value['PrPr_Price'];
			$subtotal += $price;
		}
		return $subtotal;
	}

	// Get all products with the attached price
	function fetchProducts($order_id) {
		
		include('dbConn.php');
		$connection = $conn;

	    $stmt = $connection->prepare("SELECT op.OP_qty, p.prod_name, p.prod_sku, p.prod_weight, p.prod_I, p.prod_w, p.prod_h, p.prod_img_url, pp.PrPr_Price 
				FROM orderproduct AS op JOIN product AS p ON (op.OP_prod_id = p.prod_id) JOIN prodprices AS pp ON (p.prod_id = pp.PrPr_Prod_id) WHERE op.OP_Order_id = :OP_Order_id");
	    $stmt->bindParam(":OP_Order_id", $bind_order_id);
	    $bind_order_id = $order_id;
	    
	    $stmt->execute();
	    if ($stmt->rowCount() > 0) {
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach ($result as $key => $value) {
				echo "<div class='col-sm-3'>
						<div class='row'> <div class='col-sm-12'>

					<img src='".$value['prod_img_url']."' alt='' style='height:60px; width:60px;'>
					</div>
					</div>
				</div>
				<div class='col-sm-6'>
					<div class='row'>
					<div class='col-sm-12'>
					<p> <b>". $value['OP_qty']." x</b> ".$value['prod_name']."</p>
					<p>".$value['prod_sku']."</p>
					</div>
					</div>
				</div>
				<div class='col-sm-3'>
					<div class='row'>
					<div class='col-sm-12'>
					<p style='text-align:right; height:60;'><b>$".$value['PrPr_Price']."</b></p>
					</div>
					</div>
				</div><hr>";
			}

			return $result;
		} 
		$connection = null;
		$stmt = null;
	}

 ?>

 <html>
 	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>COMP344 Assignment 2 2016, Checkout System [Core] </title>
	  	<meta charset="utf-8">
  	  	<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  		
		<script src="jquery-sdk/jquery-1.12.2.min.js"></script>
		<script src="jquery-sdk/jquery-ui.js"></script>
		<script>
			$(document).ready(function() {
				
				// handle functionality of text_area that gets user notes informations
				var text_max = 200;

				$('#count_message').html(text_max + ' remaining');

				$('#text').keyup(function() {
					// Calculate text length
				  	var text_length = $('#text').val().length;
				  	// Calculate remaining text 
				  	var text_remaining = text_max - text_length;
				  
				  // Update message under text area
				  $('#count_message').html(text_remaining + ' remaining');
				});

				// Back button send user to the previous step 
				$("#backBtn").on('click', function() {
					window.location.replace("shippingMethod.php");
				})

				// Continue button sends user to the next step of checkout process
				$("#continueBtn").on('click', function() {
					window.location.replace("paymentMethod.php");
				})


			});
		</script>
		<script type="text/javascript">

			function validate() {
				return true;
			}

		</script>
 	</head>
 	<body>
 		<div class="jumbotron text-center">
  			<h1>Checkout</h1>
  			<h3>Order summary</h3>
		</div>
		<div class="container">
				<ul class="breadcrumb">
					<!-- TODO: Add reference to the Shopping Cart System here-->
				    <li><a href="#">Cart</a></li>
				    <li><a href="index.php">Billing details</a></li>
				    <li><a href="shippingMethod.php">Payment & delivery</a></li>
				    <li class="active">Order summary</li>
				    <li><a href="paymentMethod.php">Payment</a></li>
				</ul>
				<div class="row">
					<div class="col-sm-4">
						<h4>Shipping $ billing info</h4>
						<!-- <a href="index.php">(Edit)</a> -->
						<div class="row">
							<div class= "col-sm-12">
								
								<div class="row">
									<div class="col-sm-12">
										<label>Billing Address</label>
										<?php 

											$billing_address = getBillingAddress($order_id);											
											$fname = $billing_address['sh_firstname'];
											$lname = $billing_address['sh_familyname'];
											$formatted_bill_address = formatAddress($billing_address);

											echo '<p>'. $fname . ' '. $lname .'</p>';
	    									echo "<p>" . $formatted_bill_address . "</p>";
											
										 ?>
									</div>
									<div class="col-sm-12">
										<label>Shipping Address</label>
										<?php 
											$shipping_address = getShippingAddress($order_id);

											if(empty($shipping_address)) {
												$shipping_address = getBillingAddress($order_id);
											}
											$sh_fname = $shipping_address['sh_firstname'];
											$sh_lname = $shipping_address['sh_familyname'];
											$sh_postcode = $shipping_address['sh_postcode'];
											$formatted_sh_address = formatAddress($shipping_address);

											// echo '<p>'. $sh_fname . ' '. $sh_lname .'</p>';
	    									echo "<p>" . $formatted_sh_address . "</p>";
											
										 ?>
									</div>
									<?php 	fetchShopperInfo("2"); ?>
									<div class="col-sm-12">
										<label>Note</label>
										<p><?php echo $deliveryNotes; ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<h4>Payment method</h4>
						<!-- <a href="shippingMethod.php">(Edit)</a> -->
						<div class="row">
							<div class= "col-sm-12">
								<label><?php 
									if ($paymentMethod === '1') {
										echo 'Credit Card';
									}
									else if ($paymentMethod === '2') {
										echo 'Debit Card';
									}
									else if ($paymentMethod === '3') {
										echo 'PayPal';
									}
								 ?></label>
							</div>
						</div>
						<h4>Shipping method</h4>
						<!-- <a href="shippingMethod.php">(Edit)</a> -->
						<p>
						<?php 
							$FROM_POSTCODE = getenv('AUS_POST_API_FROM_POSTCODE');
							$shipping_cost = calculateShippingCost($FROM_POSTCODE, $sh_postcode, 15, 4, 5, 3.4, $shippingCode);  
						?>
						</p>
					</div>
					<div class="col-sm-4">
						<h4>Ordered items</h4>
						<div class="row">
							<div class="col-sm-12">
								<?php  
								// Populate prodcuts from the DB
								$products = fetchProducts($order_id); ?>
								<br>
								<hr>
								<br>
								<div class='row'>
									<div class='col-sm-6' style="margin-top: 30px;">
										<p>Subtotal</p>			
									</div>
									<div class='col-sm-6' style="margin-top: 30px;">
										<p style="text-align: right;">
											<?php 
												$subtotal = calculateSubtotal($products);
												echo '$'.$subtotal 
											?>
										</p>
									</div>
								</div>
								<div class='row'>
									<div class='col-sm-6'>
										<p>Tax</p>			
									</div>
									<div class='col-sm-6'>
										<p style="text-align: right;">
											<?php
												$tax_amount = calculateTaxAmount($products);
												echo '$'.calculateTaxAmount($products); 
											?>
										</p>
									</div>
								</div>
								<div class='row'>
									<div class='col-sm-6'>
										<p>Shipping</p>
									</div>
									<div class='col-sm-6'>
										<p style="text-align: right;"><?php echo '$'.$shipping_cost ?></p>
									</div>
								</div>
								<div class='row'>
									<div class='col-sm-6'>
										<h4>Total</h4>			
									</div>
									<div class='col-sm-6'>
										<h4 style="text-align: right;">
											<?php 
												$total_amount = calculateTotalAmount($products, $shipping_cost);
												echo '$'.calculateTotalAmount($products, $shipping_cost); 

												updateOrderTotal($order_id, $total_amount, $tax_amount, $subtotal);	
											?>
										</h4>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12" style="height=60px; margin-bottom:60px;">
						<hr>
						<div class='row'>
							<input class='col-sm-4 btn btn-secondary' type="button" value="Back to Payment $ delivery" id="backBtn">
							<div class="col-sm-4"></div>
							<input class='col-sm-4 btn btn-primary ' type="button" value="Continue to payment" id="continueBtn">
						</div>
					</div>
				</div>
		</div>
 	</body>
</html>